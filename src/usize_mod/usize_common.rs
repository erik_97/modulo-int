use std::marker::PhantomData;

use crate::size::{SizeOfMod, SmallMod};

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct ModUsize<const M: usize, S: SizeOfMod = SmallMod> {
    pub(super) value: usize,
    pub(super) phantom: PhantomData<S>,
}
impl<const M: usize, S: SizeOfMod> PartialEq<usize> for ModUsize<M, S> {
    fn eq(&self, other: &usize) -> bool {
        other % M == self.value
    }
}
#[cfg(test)]
mod tests {
    use super::ModUsize;

    #[test]
    fn usize_equality_regular_usize() {
        let mut mod_int = ModUsize::<9>::new(0);
        for i in 0..100 {
            assert_eq!(mod_int, i);
            mod_int = mod_int + 1;
        }
    }
}
