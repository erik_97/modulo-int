use std::{
    marker::PhantomData,
    ops::{Add, AddAssign},
};

use crate::size::LargeMod;

use super::usize_common::ModUsize;

impl<const M: usize> ModUsize<M, LargeMod> {
    pub fn new(value: usize) -> Self {
        assert!(
            M > i32::MAX as usize,
            "M={M} is too small, use ModUsize<{M}, SmallMod> instead"
        ); // TODO: make this a static check
        Self {
            value: value % M,
            phantom: PhantomData,
        }
    }
}

impl<const M: usize> Add for ModUsize<M, LargeMod> {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let (value, overflow) = usize::overflowing_add(self.value, other.value);

        let value = if overflow {
            value + (usize::MAX - M) + 1 // Add lost value from overflow, can never be larger than M
        } else if value >= M {
            value - M // If larger than M, subtract M
        } else {
            value
        };
        debug_assert!(value < M);

        Self {
            value: value % M,
            phantom: PhantomData,
        }
    }
}

impl<const M: usize> Add<usize> for ModUsize<M, LargeMod> {
    type Output = Self;

    fn add(self, rhs: usize) -> Self::Output {
        let rhs = Self::new(rhs);
        self + rhs
    }
}

impl<const M: usize> AddAssign for ModUsize<M, LargeMod> {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs;
    }
}

#[cfg(test)]
mod tests {
    use crate::{size::LargeMod, usize_mod::usize_common::ModUsize};

    #[test]
    fn usize_large_add_normal() {
        const MOD: usize = usize::MAX - 10;
        let result = ModUsize::<MOD, LargeMod>::new(MOD - 20) + ModUsize::<MOD, LargeMod>::new(13);
        assert_eq!(result, MOD - 7);
    }

    #[test]
    fn usize_large_add_wrapping() {
        const MOD: usize = usize::MAX / 2 + 10;
        let result =
            ModUsize::<MOD, LargeMod>::new(MOD - 50) + ModUsize::<MOD, LargeMod>::new(MOD - 44);
        assert_eq!(result, MOD - 94);
    }

    #[test]
    fn usize_large_add_wrapping_overflow() {
        const MOD: usize = usize::MAX - 10;
        let result =
            ModUsize::<MOD, LargeMod>::new(MOD - 9) + ModUsize::<MOD, LargeMod>::new(MOD - 13);
        assert_eq!(result, MOD - 22);
    }

    #[test]
    fn usize_large_add_regular_usize() {
        const MOD: usize = usize::MAX - 100;
        for i in 1..100 {
            assert_eq!(
                ModUsize::<MOD, LargeMod>::new(i + usize::MAX / 2),
                ModUsize::<MOD, LargeMod>::new(usize::MAX / 2) + i
            );
        }
    }

    #[test]
    fn usize_large_add_asign_normal() {
        const MOD: usize = usize::MAX - 100;
        let mut value = ModUsize::<MOD, LargeMod>::new(MOD - 4);
        value += ModUsize::<MOD, LargeMod>::new(2);
        assert_eq!(ModUsize::<MOD, LargeMod>::new(MOD - 2), value);
    }

    #[test]
    fn usize_large_add_asign_wrapping() {
        const MOD: usize = usize::MAX - 100;
        let mut value = ModUsize::<MOD, LargeMod>::new(MOD - 4);
        value += ModUsize::<MOD, LargeMod>::new(6);
        assert_eq!(ModUsize::<MOD, LargeMod>::new(2), value);
    }
}
