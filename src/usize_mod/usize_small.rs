use std::{
    marker::PhantomData,
    ops::{Add, AddAssign},
};

use crate::size::SmallMod;

use super::usize_common::ModUsize;

impl<const M: usize> ModUsize<M, SmallMod> {
    pub fn new(value: usize) -> Self {
        assert!(
            M <= i32::MAX as usize,
            "M={M} is too large, use ModUsize<{M}, LargeMod> instead"
        ); // TODO: make this a static check
        assert_ne!(M, 0, "M cannot be 0"); // TODO: make this a static check
        assert_ne!(M, 1, "M cannot be 1"); // TODO: make this a static check
        Self {
            value: value % M,
            phantom: PhantomData,
        }
    }
}

impl<const M: usize> Add for ModUsize<M, SmallMod> {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let value = self.value + other.value;
        let value = if value >= M { value - M } else { value };
        debug_assert!(value < M);

        Self::Output {
            value,
            phantom: PhantomData,
        }
    }
}

impl<const M: usize> Add<usize> for ModUsize<M, SmallMod> {
    type Output = Self;

    fn add(self, rhs: usize) -> Self::Output {
        let rhs = Self::new(rhs);
        self + rhs
    }
}

impl<const M: usize> AddAssign for ModUsize<M, SmallMod> {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs;
    }
}

impl<const M: usize> PartialEq<i32> for ModUsize<M, SmallMod> {
    fn eq(&self, other: &i32) -> bool {
        other.rem_euclid(M as i32) as usize == self.value
    }
}

#[cfg(test)]
mod tests {
    use crate::usize_mod::usize_common::ModUsize;

    #[test]
    #[should_panic]
    fn usize_zero_should_panic() {
        // TODO: should not compile
        let _ = ModUsize::<0>::new(4);
    }

    #[test]
    #[should_panic]
    fn usize_one_should_panic() {
        // TODO: should not compile
        let _ = ModUsize::<1>::new(4);
    }

    #[test]
    fn usize_small_add_normal() {
        let result = ModUsize::<9>::new(3) + ModUsize::<9>::new(5);
        assert_eq!(result, 8);
    }

    #[test]
    fn usize_small_add_wrapping() {
        let result = ModUsize::<9>::new(7) + ModUsize::<9>::new(4);
        assert_eq!(result, 2);
    }

    #[test]
    fn usize_small_add_regular_usize() {
        const MOD: usize = 7;
        for i in 1..10 {
            assert_eq!(ModUsize::<MOD>::new(i), ModUsize::<MOD>::new(0) + i);
        }
    }

    #[test]
    fn usize_small_add_asign_normal() {
        const MOD: usize = 7;
        let mut value = ModUsize::<MOD>::new(4);
        value += ModUsize::<MOD>::new(2);
        assert_eq!(ModUsize::<MOD>::new(6), value);
    }

    #[test]
    fn usize_small_add_asign_wrapping() {
        const MOD: usize = 7;
        let mut value = ModUsize::<MOD>::new(4);
        value += ModUsize::<MOD>::new(2);
        assert_eq!(ModUsize::<MOD>::new(6), value);
    }

    #[test]
    fn usize_small_equality_i32() {
        let value = ModUsize::<7>::new(6);
        assert_eq!(value, -1);
        assert_eq!(value, -8);
        assert_eq!(value, 6);
        assert_eq!(value, 13);
    }
}
