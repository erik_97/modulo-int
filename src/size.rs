#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct SmallMod;
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct LargeMod;

mod private {
    use super::{LargeMod, SmallMod};

    pub trait Sealed {}

    impl Sealed for SmallMod {}
    impl Sealed for LargeMod {}
}

pub trait SizeOfMod: private::Sealed {}

impl SizeOfMod for SmallMod {}
impl SizeOfMod for LargeMod {}
